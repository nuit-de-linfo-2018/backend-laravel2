<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();


Route::get('/', 'HomeController@index')->name('home');
Route::get('/polling/test', 'PollingController@index');

Route::get('/pipboy', 'HomeController@pipboy')->name('pipboy');
Route::get('/pipboy-env', 'HomeController@pipboy_env')->name('pipboy-env');
Route::get('/pipboy-dev', 'HomeController@pipboy_dev')->name('pipboy-dev');

Route::resource('user', 'UserController');