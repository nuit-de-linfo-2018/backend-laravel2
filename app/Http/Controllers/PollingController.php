<?php

namespace App\Http\Controllers;

use App\Data;
use Illuminate\Support\Facades\DB;

class PollingController extends Controller
{

    /**
     *
     */
    public function index()
    {
        $data = Data::orderBy('id', 'DESC')->limit(5)->get();
        return $data->toJson();
    }
}
