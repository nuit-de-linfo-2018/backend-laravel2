<?php

namespace App\Http\Controllers;

use App\History;
use Illuminate\Http\Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function pipboy()
    {
        return view('stats');
    }

    public function pipboy_env()
    {
        return view('env');
    }

    public function pipboy_dev()
    {
        return view('devices');
    }
}
