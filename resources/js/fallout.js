$(document).ready(function () {
    poll();
});

function poll() {
    $.ajax({
        url: 'https://goat.agravelot.eu/polling/test',
        type: 'get',
        header: {
            'Access-Control-Allow-Origin': '*',
        },
        success: function (response) {
            response = JSON.parse(response);

            $('#fq').text(response['value']);
            setTimeout(poll, 3);
        }
    })
}