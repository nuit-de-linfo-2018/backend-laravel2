@extends('layouts.pipboy')

@section('content')
    <div class="pipboy-content mCustomScrollbar" data-mcs-theme="rounded-dots-dark">
        <h1>PIPBOY</h1>
        <div class="row">
            <div class="col-md-4"><img src="{{ asset('img/vault_boy.png') }}" class="img-responsive" alt="vault_boy">
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-4"><p>Fréquence cardiaque</p></div>
                    <div class="col-md-4">120</div>
                </div>
                <div class="row">
                    <div class="col-md-4">Température</div>
                    <div class="col-md-4">37</div>
                </div>
                <div class="row">
                    <div class="col-md-4">Niveau d'hydratation</div>
                    <div class="col-md-4">78%</div>
                </div>
                <div class="row">
                    <div class="col-md-4">Tension</div>
                    <div class="col-md-4">7.5/13</div>
                </div>
                <div class="row">
                    <div class="col-md-4">Fréquence respiratoire</div>
                    <div class="col-md-4">20</div>
                </div>
            </div>
        </div>
    </div>
    <div class="pipboy-button-holder">
        <a href="{{ route('pipboy') }}"><img src="{{ asset('img/button.png') }}"></a>
        <a href="{{ route('pipboy-env') }}"><img src="{{ asset('img/button-lit.png') }}"></a>
        <a href="{{ route('pipboy-dev') }}"><img src="{{ asset('img/button.png') }}"></a>
    </div>
@endsection
