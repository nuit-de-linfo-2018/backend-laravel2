<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'PipBoy') . ' - PipBoy' }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    {{--<script src="{{ asset('js/jquery.mCustomScrollbar.js') }}"></script>--}}
    <script src="{{ asset('js/fallout.js') }}"></script>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}">
    {{--<link href="{{ asset('css/jquery.mCustomScrollbar.css') }}">--}}
    <link href="{{ asset('css/fallout.css') }}" rel="stylesheet">
    <link href="{{ asset('css/pipboy-adjust.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        <div class="container-fluid">
            <img id="pipboy" src="{{ asset('img/Pip-Boy_3000_GOATS.png') }}">
            @yield('content')
        </div>
    </div>
</body>

</html>
