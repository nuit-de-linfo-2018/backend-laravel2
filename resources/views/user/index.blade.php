@extends('layouts.app')

@section('content')
<main class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="row justify-content-center card-sum">

        <div class="card text-center col-md-3 card-user">
            <div class="row">
                <div class="col-md-6">
                    <p class="i-centered meteo"><i class="fas fa-cloud-sun fa-5x"></i></p>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <p class="text-half-card">Météo</p>
                    </div>
                    <div class="row">
                        <p class="text-half-card">Actuelle</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-1"></div>

        <div class="card col-md-3 card-user">
            <div class="row">
                <div class="col-md-6">
                    <p class="i-centered shoe"><i class="fas fa-shoe-prints fa-rotate-270 fa-5x"></i></p>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <p class="text-half-card">66</p>
                    </div>
                    <div class="row">
                        <p class="text-half-card">pas</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-1"></div>

        <div class="card text-center col-md-3 card-user">
            <div class="row">
                <div class="col-md-6">
                    <p class="i-centered gps"><i class="fas fa-location-arrow fa-5x"></i></p>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <p class="text-half-card">48°51'4581"</p>
                    </div>
                    <div class="row">
                        <p class="text-half-card">48°51'4581"</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--</main>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">-->
    <div class="row card-sum">
        <div class="col-6">
            <div class="card cards-sum">
                <canvas id="card-temp"></canvas>
            </div>
        </div>
        <div class="col-6">
            <div class="card cards-sum">
                <canvas id="speed"></canvas>
            </div>
        </div>
    </div>
</main>

@endsection

@section('javascript')
<script>

    //$.ajax({
    //    url: "https://goat.agravelot.eu/api/data",
    //}).fail(function(err) {
    //    console.log(err);
    //}).done(function(data) {
    //    console.log(data);
    //    let 
    //});

    var lineChartData = {
        labels: ['00h', '01h', '02h', '03h', '04h', '05h', '06h', '07h', '08h', '09h', '10h', '11h', '12h', '13h',
            '14h', '15h', '16h', '17h', '18h', '19h', '20h', '21h', '22h', '23h'
        ],
        datasets: [{
            label: 'Fréquence cardiaque',
            borderColor: "#FF0000",
            backgroundColor: "#FF0000",
            fill: false,
            data: [
                90,
                100,
                104,
                99,
                95,
                80,
                89,
                92,
                94,
                120,
                90,
                100,
                104,
                99,
                95,
                80,
                89,
                92,
                94,
                120,
                80,
                95,
                70,
                73
            ],
            yAxisID: 'y-axis-1',
        }, {
            label: 'Température corporelle',
            borderColor: "#0000FF",
            backgroundColor: "#0000FF",
            fill: false,
            data: [
                37,
                37,
                37,
                37,
                38,
                38,
                37,
                37,
                37,
                37,
                38,
                38,
                37,
                37,
                37,
                37,
                38,
                38,
                37,
                37,
                37,
                37,
                38,
                38
            ],
            yAxisID: 'y-axis-2',
        }]
    };

    window.onload = function () {
        var ctx = document.getElementById('card-temp').getContext('2d');
        window.myLine = Chart.Line(ctx, {
            data: lineChartData,
            options: {
                responsive: true,
                hoverMode: 'index',
                stacked: true,
                title: {
                    display: false,
                    text: 'Fréquence cardiaque & Température corporelle'
                },
                scales: {
                    yAxes: [{
                        type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: 'left',
                        id: 'y-axis-1',
                        ticks: {
                            beginAtZero: false,
                            callback: function (value, index, values) {
                                return value + " bpm";
                            }
                        },
                    }, {
                        type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: 'right',
                        id: 'y-axis-2',
                        ticks: {
                            beginAtZero: false,
                            callback: function (value, index, values) {
                                return value + "°C";
                            }
                        },

                        // grid line settings
                        gridLines: {
                            drawOnChartArea: false, // only want the grid lines for one axis to show up
                        },
                    }],
                },
            }
        });
        var ctxSpeed = document.getElementById('speed').getContext('2d');
        window.myLine = new Chart(ctxSpeed, config);
    };

    var config = {
        type: 'line',
        data: {
            labels: ['00h', '01h', '02h', '03h', '04h', '05h', '06h', '07h', '08h', '09h', '10h', '11h', '12h',
                '13h',
                '14h', '15h', '16h', '17h', '18h', '19h', '20h', '21h', '22h', '23h'
            ],
            datasets: [{
                label: 'Vitesse',
                borderColor: "#FF0000",
                backgroundColor: "rgba(255, 0, 0, 0.2)",
                data: [
                    8, 8, 8, 7, 7, 4, 4, 0, 0, 0, 0, 0, 0, 0, 5, 11, 12, 12, 9, 7, 3, 5, 6, 7
                ],
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Chart.js Line Chart - Stacked Area'
            },
            tooltips: {
                mode: 'index',
            },
            hover: {
                mode: 'index'
            },
            scales: {
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Heure'
                    }
                }],
                yAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Vitesse'
                    },
                    ticks: {
                        beginAtZero: false,
                        callback: function (value, index, values) {
                            return value + " km/h";
                        }
                    },
                }]
            },

            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';
                        console.log(tooltipItem.index);
                        return label + " " + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] +
                            "km/h";
                    }
                }
            }
        }
    };

    

</script>
@endsection