@extends('layouts.pipboy')

@section('content')
    <div class="pipboy-content">
        <h1>PIPBOY</h1>
        <div class="row">
            <div class="col-md-4"><img src="{{ asset('img/vault_boy.png') }}" class="img-responsive vault-boy" alt="vault_boy">
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div>
                        Fréquence cardiaque : <span id="fq">120</span>
                        <div class="monitor"> <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 60" style="enable-background:new 0 0 500 200;" xml:space="preserve"> <g> <polyline class="ekg" points="100,30 75,30 65,50 45,10 35,30 0,30 "/> </g> </svg> </div>
                    </div>
                </div>
                <div class="row">
                    <div>
                        Température : <span id="temp">37</span><span>°</span>
                    </div>
                </div>
                <div class="row">
                    <div>
                        Niveau d'hydratation : <span id="hygro">78</span><span>%</span>
                    </div>
                </div>
                <div class="row">
                    <div>
                        Tension : <span id="tens">7.5</span><span>/13</span>
                    </div>
                </div>
                <div class="row">
                    <div>
                        Fréquence respiratoire : <span id="fq-r">20</span><span>/min</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pipboy-button-holder">
        <a href="{{ route('pipboy') }}"><img src="{{ asset('img/button-lit.png') }}"></a>
        <a href="{{ route('pipboy-env') }}"><img src="{{ asset('img/button.png') }}"></a>
        <a href="{{ route('pipboy-dev') }}"><img src="{{ asset('img/button.png') }}"></a>
    </div>
@endsection
