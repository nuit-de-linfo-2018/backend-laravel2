@extends('layouts.app')

@section('content')
<div class="row">
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="cards-sum">
            <div class="list-group">
                <a href="/user" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                    [Mission name] Space Y <span class="badge badge-primary badge-pill">RAS</span>
                </a>
                <a href="/user" class="list-group-item list-group-item-action list-group-item-warning d-flex justify-content-between align-items-center">
                    [Mission name] Naso
                    <span class="badge badge-warning badge-pill">Message d'alerte</span>
                </a>
                <a href="/user" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                    [Mission name] ASI <span class="badge badge-primary badge-pill">RAS</span>
                </a>
                <a href="/user" class="list-group-item list-group-item-action list-group-item-danger d-flex justify-content-between align-items-center">
                    [Mission name] ROCOSMOS <span class="badge badge-danger badge-pill">Message d'erreur</span>
                </a>
                <a href="/user" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                    [Mission name] CCNES <span class="badge badge-primary badge-pill">RAS</span>
                </a>
            </div>
        </div>
    </main>
</div>
@endsection
