$(document).ready(function () {
    poll();
});

function poll() {
    $.ajax({
        url: 'https://goat.agravelot.eu/polling/test',
        type: 'get',
        success: function (response) {

            response = JSON.parse(response);
            let data = JSON.parse(response[0]['value']);

            $('#fq').text(data['frcardiaque']);
            $('#fq-r').text(data['frrespiratoire']);
            $('#temp').text(data['temp']);
            $('#hygro').text(data['hydratation']);
            $('#tens').text(data['tension']);
            setTimeout(poll, 1500);
        }
    })
}