<?php

use App\Data;
use Faker\Generator as Faker;

$factory->define(Data::class, function (Faker $faker) {
    return [
        'type' => $faker->word,
        'device' => $faker->word,
        'value' => $faker->word,
        'name' => $faker->text,
    ];
});
