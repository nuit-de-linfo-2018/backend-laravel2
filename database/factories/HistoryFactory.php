<?php

use Faker\Generator as Faker;

$factory->define(App\History::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'data_id' => 1,
        'time' => \Carbon\Carbon::now(),
        'value' => $faker->numberBetween(0,10),
    ];
});
